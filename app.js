var linebot = require('linebot');
 
var bot = linebot({
  channelId: 1654049004,
  channelSecret: '0bb88a0216e44139765f1eb2d916d232',
  channelAccessToken: 'OkyzssmpLzNxg7vlJw13yXxZqskQeY2+yFiPbERGkLmM1zgxxiDpN6BfiRaVoMftzqCs81BgGwWoV1Syv43h7uSEPSGG/+0HsAFSQaBH7sht9+mIT5C10SeJs0Lrf8zQ5g/u0lx+biBkVsdAyoHg4gdB04t89/1O/w1cDnyilFU='
});
 
bot.on('follow', event => {
  event.reply({"type":"flex","altText":"嗨～這是一個用來測試．．．","contents":{"type":"bubble","size":"giga","body":{"type":"box","layout":"vertical","contents":[{"type":"text","text":"hello,world","contents":[{"type":"span","text":"嗨～這是一個用來測試 "},{"type":"span","text":"Flex Message","color":"#ff005e","weight":"bold","size":"md"},{"type":"span","text":"的機器人"}],"wrap":true},{"type":"text","text":"使用方法如下：","margin":"md","size":"sm"},{"type":"text","text":"1. 前往製作 FlexMessage 的網站","size":"sm","contents":[{"type":"span","text":"1. 前往製作"},{"type":"span","text":"FlexMessage ","color":"#ff005e","weight":"bold"},{"type":"span","text":"的網站"}],"margin":"sm"},{"type":"text","size":"sm","contents":[{"type":"span","text":"2. 貼上"},{"type":"span","text":"FlexMessage ","color":"#ff005e","weight":"bold"},{"type":"span","text":"的"},{"type":"span","text":"JSON ","color":"#ff005e","weight":"bold"},{"type":"span","text":"給我吧"}],"wrap":true},{"type":"text","text":"hello,world","contents":[{"type":"span","text":"3. 格式沒錯的話，會回傳對應的預覽訊息"}],"size":"sm","wrap":true},{"type":"text","text":"hello,world","contents":[{"type":"span","text":"4. 若長度允許，會一併回傳包裝好的 FlexMessage"}],"size":"sm","wrap":true}]},"footer":{"type":"box","layout":"vertical","contents":[{"type":"button","action":{"type":"uri","label":"網址點我","uri":"https://developers.line.biz/flex-simulator/"},"style":"secondary"}]}}})
})
bot.on('message', function (event) {
  if (event.message.type === 'text') {
    if (isJson(event.message.text)) {
      // is JSON
      let message = JSON.parse(event.message.text)

      if (message.type === 'bubble' || message.type === 'carousel') {
        // 未包裝的 Flex Message
        let flexMessage = {
          type: 'flex',
          altText: '請修改這邊的文字',
          contents: message
        }
        let flexMessageText = JSON.stringify(flexMessage)
        if (flexMessageText.length < 2000) {
          event.reply([flexMessage, flexMessageText])
        } else {
          event.reply(flexMessage)
        }
        
      }
      if (message.type === 'flex') {
        // 完整的 Flex message
        event.reply(message)
      }
    } else {
      // not JSON
      event.reply({"type":"flex","altText":"[錯誤]","contents":{"type":"bubble","body":{"type":"box","layout":"vertical","contents":[{"type":"text","text":"hello,world","contents":[{"type":"span","text":"[錯誤]","color":"#ff0000","size":"lg","weight":"bold"},{"type":"span","text":"JSON 格式有誤","weight":"bold","size":"md"}],"wrap":true },{"type":"text","text":"hello,world","contents":[{"type":"span","text":"＊若忘記製作網站的網址可以點擊下方按鈕"}],"size":"sm","wrap":true,"margin":"md"}]},"footer":{"type":"box","layout":"vertical","contents":[{"type":"button","action":{"type":"uri","label":"網址點我","uri":"https://developers.line.biz/flex-simulator/"},"style":"secondary"}]}}})
    }
  } else {
    event.reply({"type":"flex","altText":"[錯誤]","contents":{"type":"bubble","body":{"type":"box","layout":"vertical","contents":[{"type":"text","text":"hello,world","contents":[{"type":"span","text":"[錯誤]","color":"#ff0000","size":"lg","weight":"bold"},{"type":"span","text":"JSON 格式有誤","weight":"bold","size":"md"}],"wrap":true },{"type":"text","text":"hello,world","contents":[{"type":"span","text":"＊若忘記製作網站的網址可以點擊下方按鈕"}],"size":"sm","wrap":true,"margin":"md"}]},"footer":{"type":"box","layout":"vertical","contents":[{"type":"button","action":{"type":"uri","label":"網址點我","uri":"https://developers.line.biz/flex-simulator/"},"style":"secondary"}]}}})
  }
});

function isJson(str) {
  try {
      JSON.parse(str);
  } catch (e) {
      return false;
  }
  return true;
}
 
bot.listen('/webhook', 3088);
